<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

  public function index()
  {
    return view('Home/index', ['data' => 'Samantha']);
  }

  public function category()
  {
    return view('Home/category', ['data' => 'Samantha']);
  }
}