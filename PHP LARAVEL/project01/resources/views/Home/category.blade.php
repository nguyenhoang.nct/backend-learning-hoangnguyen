@extends('Layouts.default')

@section('title', 'Category page')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
    <p>Category page</p>
@endsection

@section('content')
    <p>This is my body content.</p>
@endsection