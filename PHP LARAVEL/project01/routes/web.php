<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/category', 'HomeController@category')->name('home.category');

// tra ve View cua page
// Route::get('test-view', function() {
//     return view('frontend.test-view');
// });

// de tranh truong hop mot view khong ton tai, ta testview,
// truong hop nay la khong ton tai view "contact" nen trang bao loi
Route::get('contact', function(){
    if (View::exists('frontend.contact')) {
        return view('frontend.contact');
    } else {
      return 'Trang liên hệ đang bị lỗi, bạn vui lòng quay lại sau';
    }
});

// Route::get('/news-detail', function(){
//     if (View::exists('app.Http.Controllers.MainController')) {
//         return view('app.Http.Controllers.MainController')->with(['news_id' => $news_id, 'news_title' => $news_title]);
//     } else {
//       return 'Trang liên hệ đang bị lỗi, bạn vui lòng quay lại sau';
//     }
// });

Route::get('/news-detail/{news_id_string}', 'MainController@showNews');