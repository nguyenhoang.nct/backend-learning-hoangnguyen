<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Login</title>

	<!-- Custom fonts for this template-->
	<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
<?php

	session_start();
	$noti_empty ='';
	$noti_error ="";
	//Gọi file connection.php ở bài trước
	require("./_connect_db.php");
	// Kiểm tra nếu người dùng đã ân nút đăng nhập thì mới xử lý
	if (isset($_POST["btn_login"])) {
		// lấy thông tin người dùng
		$username = $_POST["username"];
		$password = $_POST["password"];

		//làm sạch thông tin, xóa bỏ các tag html, ký tự đặc biệt
		//mà người dùng cố tình thêm vào để tấn công theo phương thức sql injection
		$username = strip_tags($username);
		$username = addslashes($username);
		$password = strip_tags($password);
		$password = addslashes($password);
		
		// thêm muối cho password
		$salt = 'abc';
		$password = md5(($salt.$password), false);

		if ($username == "" || $password =="") {
			$noti_empty = "Username or Password not empty!";
		} else {
			$sql = "select * from users where username = '$username' and password = '$password' ";
			$query = mysqli_query($connection,$sql);
				$num_rows = mysqli_num_rows($query);
			if ($num_rows==0) {
				$noti_error = "Username or Password not true!";
			} else {
				//tiến hành lưu tên đăng nhập vào session để tiện xử lý sau này
				$_SESSION['username'] = $username;
				// Thực thi hành động sau khi lưu thông tin vào session
				// ở đây mình tiến hành chuyển hướng trang web tới một trang gọi là index.php
				header('Location:index.php');
			}
		}
	}
?>

	<div class="container">

		<!-- Outer Row -->
		<div class="row justify-content-center">

			<div class="col-xl-10 col-lg-12 col-md-9">

				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<form method="POST" action="login.php">
							<div class="row">
								<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
								<div class="col-lg-6">
									<div class="p-5">
										<div class="text-center">
											<h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
											<h1 class="h4 text-gray-900 mb-4" style="color: red"><?=$noti_empty;?></h1>
											<h1 class="h4 text-gray-900 mb-4" style="color: red"><?=$noti_error;?></h1>
										</div>
										<form class="user">
											<div class="form-group">
												<input type="text" class="form-control form-control-user" id="exampleFirstName" name="username" placeholder="UserName">
											</div>
											<div class="form-group">
												<input type="password" class="form-control form-control-user" id="exampleInputPassword" name="password" placeholder="Password">
											</div>
											<div class="form-group">
												<div class="custom-control custom-checkbox small">
													<input type="checkbox" class="custom-control-input" id="customCheck">
													<label class="custom-control-label" for="customCheck">Remember Me</label>
												</div>
											</div>
											<input class="btn btn-primary btn-user btn-block" name="btn_login" type="submit" value="Login">
											<hr>
											<a href="index.html" class="btn btn-google btn-user btn-block">
												<i class="fab fa-google fa-fw"></i> Login with Google
											</a>
											<a href="index.html" class="btn btn-facebook btn-user btn-block">
												<i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
											</a>
										</form>
										<hr>
										<div class="text-center">
											<a class="small" href="forgot-password.html">Forgot Password?</a>
										</div>
										<div class="text-center">
											<a class="small" href="register.php">Create an Account!</a>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>

	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

</body>

</html>
