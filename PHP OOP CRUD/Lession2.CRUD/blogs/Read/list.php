
<?php
	// connection database
	include('../../_connect_db.php');
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>List</title>
	<link rel="stylesheet" href="./list_style.css">
</head>
<body>
	<?php
	// Hien thi danh sach bai viet (chua xoa):
		$sql = "SELECT * FROM  blogs WHERE deleted_at is null";
		$result = mysqli_query($connection, $sql);
		echo "<h2> Số bài viết chưa xoá: ".mysqli_num_rows($result)."<h2>";
	?>
	<h3>Danh sách các bài chưa xoá:</h3>
	<table>
		<thead>
			<tr>
				<th>STT</th>
				<th>Tiêu Đề</th>
				<th>Mô tả</th>
				<th>Nội dung</th>
				<th>Ngày công bố</th>
				<th>Ngày tạo</th>
				<th>Ngày update</th>
				<th>Trạng thái</th>
				<th>Category_id</th>
				<th>Ngày xoá</th>
			</tr>
		</thead>
		<tbody>
			<?php
				include('_fetch_data.php');
			?>
		</tbody>
	</table>
	<br>


	<?php
	// Hien thi danh sach cac bai viet da bi xoa nhung chua xoa han khoi DB.
		$deleted = "SELECT * FROM  blogs WHERE deleted_at is not null";
		$deleted_result = mysqli_query($connection, $deleted);
		echo "<h2> Số bài viết đã xoá: ".mysqli_num_rows($deleted_result)."</h2>";
	?>
	<h3>Danh sách các bài đã xoá:</h3>
	<table>
		<thead>
			<tr>
				<th>STT</th>
				<th>Tiêu Đề</th>
				<th>Mô tả</th>
				<th>Nội dung</th>
				<th>Ngày công bố</th>
				<th>Ngày tạo</th>
				<th>Ngày update</th>
				<th>Trạng thái</th>
				<th>Category_id</th>
				<th>Ngày xoá</th>
			</tr>
		</thead>
		<tbody>
			<?php
				include('_fetch_data_deleted.php');
			?>
		</tbody>
	</table>
	<?php
		echo "<br><h2>Tổng số bài viết: ".(mysqli_num_rows($deleted_result) + mysqli_num_rows($result))."<h2><br>";
	?>
	<a href="http://bai2/blogs/Create/create.php">New write</a>
</body>
</html>