<?php 
$id = isset($_GET['id']) ? $_GET['id'] : 0;
if ($id === 0) {
 header('Location: list.php');
}
?>

<form method="POST" action="http://bai2/blogs/Update/update.php">
	<input type="hidden" name="id" value="<?php echo $id ?>" />
	<table style="border: solid 1px #000">
	<thead>
		<th>Content</th>
		<th>Type</th>
		<th>Input</th>
	</thead>
		<tbody>
			<tr>
				<td>title</td>
				<td>text</td>
				<td><input type="text" name="title" placeholder="Nhập tiêu đề" required="true"></td>
			</tr>
			<tr>
				<td>description</td>
				<td>text</td>
				<td><input type="text" name="description" placeholder="Nhập mô tả"></input></td>
			</tr>

			<tr>
				<td>content</td>
				<td>longtext</td>
				<td><input type="longtext" name="content" placeholder="Nhập nội dung"></td>
			</tr>

			<tr>
				<td>publish_date</td>
				<td>datetime</td>
				<td><input type="datetime" name="publish_date" placeholder="Thời gian công bố"></td>
			</tr>

			<tr>
				<td>created_at</td>
				<td>datetime</td>
				<td><input type="datetime" name="created_at" placeholder="Thời gian tạo"></td>
			</tr>

			<tr>
				<td>updated_at</td>
				<td>datetime</td>
				<td><input type="datetime" name="updated_at" placeholder="Thời gian sửa"></td>
			</tr>

			<tr>
				<td>status</td>
				<td>number</td>
				<td><input type="number" name="status" placeholder="Trạng thái (nhập 1 2 3 or 4)"></td>
			</tr>

			<tr>
				<td>categori_id</td>
				<td>checkbox</td>
				<td style="display: flex flex-direction: column">
					<select name="category_id">
						<?php
							include('../Create/convert_array_categories.php');
							foreach ($cates as $cates) {
								?>
								<option value="<?=$cates['id'] ?>"><?=$cates['title']?></option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>

			<tr>
				<td>deleted_at</td>
				<td>datetime</td>
				<td><input type="datetime" name="deleted_at" placeholder="Thời gian xoá"></td>
			</tr>
		</tbody>
	</table>
	<button type="submit">Save</button>
</form>