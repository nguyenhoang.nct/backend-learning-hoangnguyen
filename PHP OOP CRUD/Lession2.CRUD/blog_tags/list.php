
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Form HTML</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <h1>Danh sách bài viết</h1>
    <?php
           include('../_connect_db.php');
           $query = "SELECT * FROM blog_tags ORDER BY id";
           $result = mysqli_query($connection, $query);
           // tra ve gia tri dang bolean
           // var_dump($result);
           // echo $result;
           $total = mysqli_num_rows($result);
    ?>
    <h2><?php echo "Số lượng bài viết: " . $total;?> </h2>
    <table>
        <thead>
            <tr>
                <th>STT</th>
                <th>Blog_id</th>
                <th>Tag</th>
                <th>Ngày tao</th>
                <th>Ngày update</th>
            </tr>
        </thead>
    <tbody>
        <?php
           include('_fetch_data.php');
        ?>
    </tbody>
    </table>
    <br>
    <a href="http://blog123.com/blog_tags/create.php">Viết bài mới</a>
</body>
</html>