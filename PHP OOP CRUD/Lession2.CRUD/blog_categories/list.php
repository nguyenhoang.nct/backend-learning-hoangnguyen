<?php
include('../_connect_db.php');
session_start();
//tiến hành kiểm tra là người dùng đã đăng nhập hay chưa
//nếu chưa, chuyển hướng người dùng ra lại trang đăng nhập
if (!isset($_SESSION['username'])) {
	header('Location: ../login.php');
} else {
	echo 'Bạn đang đăng nhập với tài khoản ' . $_SESSION['username'] . "<br>";
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Form HTML</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <h1>Danh sách bài viết</h1>
    <table>
        <thead>
            <tr>
                <th>STT</th>
                <th>Tiêu Đề</th>
                <th>Mô tả</th>
                <th>Ngày viết</th>
                <th>Ngày sửa</th>
                <th>Delete</th>
                <th>Update</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include('../_connect_db.php');
            include('_fetch_data.php');
            
            ?>
        </tbody>
    </table>
    <br>
    <a href="http://blog123.com/blog_categories/create.php">Viết bài mới</a>
</body>
</html>