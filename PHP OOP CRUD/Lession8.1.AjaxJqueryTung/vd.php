<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Insert Ajax PHP & MYSQL</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
</head>

<body>


    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Insert Data in PHP using AJAX</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="well">
                            <form id="loginForm" method="" action="" novalidate="novalidate">
                                <div class="form-group">
                                    <label for="student_name" class="control-label">Tên Sinh Viên </label>
                                    <input type="text" class="form-control" id="student_name" name="student_name"
                                        value="" required="" title="Please enter you Name" placeholder="">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label for="student_age" class="control-label">Tuoi </label>
                                    <input type="text" class="form-control" id="student_age" name="student_age" value=""
                                        required="" title="Please enter your age">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label for="student_school" class="control-label">Truong DH </label>
                                    <input type="text" class="form-control" id="student_school" name="student_school"
                                        value="" required="" title="Please enter your Class">
                                    <span class="help-block"></span>
                                </div>

                                <button type="button" class="btn btn-success btn-block" name="insert-data"
                                    id="insert-data" onclick="insertData()">Insert Data</button>
                                <br>
                                <p id="message"></p>
                            </form>
                            <div id="result"></div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</body>

<script type="text/javascript">
    function insertData() {
        var student_name = $("#student_name").val();
        var student_age = $("#student_age").val();
        var student_school = $("#student_school").val();
        $.ajax({
            type: "POST",
            url: "result.php",
            data: {
                student_name: student_name,
                student_age: student_age,
                student_school: student_school
            },
            dataType: "JSON",
            success: function(data) {
                $("#message").html(data);
                $("p").addClass("alert alert-success");
                $('#result').html(data);
            },
            error: function(err) {
                alert(err);
            }
        });
    }
</script>

</html>