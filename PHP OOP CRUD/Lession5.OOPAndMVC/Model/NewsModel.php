<?php
include_once("Model.php");
include_once("NewsEntity.php");

class NewsModel extends Model
{
    public function __construct()
    {
        return parent::__construct();
    }

    public function list()
    {
        $query= 'SELECT * FROM blogs';
        $results = $this->connection->query($query);
        $news = [];
        while ($row = mysqli_fetch_assoc($results)) {
            $news[$row['id']] = new NewsEntity($row['id'], $row['title'], $row['description']);
        }
        return $news;
    }

    public function detail($id)
    {
       $query = "SELECT * FROM blogs WHERE id='" . $id . "'";
       $results = $this->connection->query($query);
       if($results)
       {
           $news = [];
           while ($row = mysqli_fetch_assoc($results)) {
               $news[] = new NewsEntity(
                   $row['id'],
                   $row['title'],
                   $row['description'],
                   $row['content'],
                   $row['publish_date'],
                   $row['created_at'],
                   $row['updated_at'],
                   $row['status'],
                   $row['category_id'],
                   $row['deleted_at'])
           }
           return isset($news[0]) ? $news[0] : false;
       } else {
           return false;
       }
    }

    public function save($news)
    {
        if(isset($news['id'])) {
            $query = "UPDATE blogs SET 
            `title`='" . $news['title'] ."', 
            `description`='" . $news['description'] ."', 
            `content`='" . $news['content'] ."', 
            `publish_date`='" . $news['publish_date'] ."', 
            `created_at`='" . $news['created_at'] ."', 
            `updated_at`='" . $news['updated_at'] ."', 
            `status`='" . $news['status'] ."', 
            `category_id`='" . $news['category_id'] ."', 
            `deleted_at`='" . $news['deleted_at'] ."' WHERE `id`=" . $news['id'];
            return $this->connection->query($query);
        } else {
            $query = "INSERT INTO blogs 
                (`title`,
                `description`,
                `content`,
                `publish_date`,
                `created_at`,
                `updated_at`,
                `status`,
                `category_id`,
                `deleted_at`) 
            VALUES 
                ('" . $news['title'] . "', 
                '" . $news['description'] . "', 
                '" . $news['content'] . "', 
                '" . $news['publish_date'] . "', 
                '" . $news['created_at'] . "', 
                '" . $news['updated_at'] . "', 
                '" . $news['status'] . "', 
                '" . $news['category_id'] . "', 
                '" . $news['deleted_at'] . "')";
            return $this->connection->query($query);
        }
    }

    public function delete($id)
    {
        $query = "DELETE FROM blogs WHERE id=" . $id;
        return $this->connection->query($query);
    }
}