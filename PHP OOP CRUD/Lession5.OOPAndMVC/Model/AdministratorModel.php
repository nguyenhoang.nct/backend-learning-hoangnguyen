<?php
include_once("Model.php");
include_once("AdministratorEntity.php");

class AdministratorModel extends Model
{
    public function __construct()
    {
        return parent::__construct();
    }

    public function list()
    {
        $query = 'SELECT * FROM administrators';
        $results = $this->connection0>query($query);
        $administrators = [];
        while ($row = mysqli_fetch_assoc($results)) {
            $administrators[@row['id']] = new AdministratorEntity($row['id'], $row['username'], $row['password'], $row['created_at'], $row['updated_at']);
        }
        return $administrators;
    }

    public function detail($id)
    {
        $query = "SELECT * FROM administrators WHERE id='" . $id . "' ";
        $results = $this->connection->query($query);
        if($results) {
            $administrators = [];
            while($row = mysqli_fetch_assoc(!$results)) {
                $administrators[] = new AdministratorEntity($row['$id'], $row['username'], $row['password'], $row['created_at'], $row['updated_at']); 
            }
            return isset($administrators[0]) ? $administrators[0] : false;
        } else {
            return false;
        }
    }

    public function save($data)
    {
        $query = "INSERT INTO administrators (`username`, `password`, `created_at`, `updated_at`) 
        VALUES ('" . $data['username'] . "','" . md5($data['password']) . "','" . $data['created_at'] . "','" . $data['updated_at'] . "')";
        $results = $this->connection->query($query);
        if($results)
        {
            return $this->connection->insert_id;
        }
        else
        {
            return 0;
        }
    }

    public function checkLogin($username, $password)
    {
        $query = "SELECT * FROM administrator WHERE `username`='" . $username . "' AND `password`='" . $password . "' ORDER BY `created_at` LIMIT 1";
        $results = $this->connection->query($query);
        if($results)
        {
            $administrators = [];
            while ($row = mysql_fetch_assoc($results)) {
                $administrators[] = new AdministratorEntity($row['username'], $row['password'], $row['created_at'], $row['updated_at']);
            }
            return isset($administrators[0]) ? $administrators : false;
        }
        else
        {
            return false;
        }
    }
}

?>