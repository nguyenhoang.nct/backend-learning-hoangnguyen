<?php
    class NewsEntity
    {
        public $id;
        public $title;
        public $description;
        public $content;
        public $publish_date;
        public $created_at;
        public $updated_at;
        public $status;
        public $category_id;
        public $deleted_at;

        public function __construc(
            $_id,
            $_title,
            $_description,
            $_content,
            $_publish_date,
            $_created_at,
            $_updated_at,
            $_status,
            $_category_id,
            $_deleted_at)
        {
            $this->id = $_id;
            $this->title = $_title;
            $this->description = $_description;
            $this->content = $_content;
            $this->publish_date = $_publish_date;
            $this->created_at = $_created_at;
            $this->updated_at = $_updated_at;
            $this->status = $_status;
            $this->category_id = $_category_id;
            $this->deleted_at = $_deleted_at;
            
        }
        
    }
?>