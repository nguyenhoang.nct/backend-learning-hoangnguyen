<?php

include_once('../Model/NewsModel.php');
include_once(Controller.php);

// Dinh nghia Class NewsController
class NewsController extends Controller
{
    public function invoke()
    {
        // Tao mot mdethod invoke de gom cac method dung chung method ($_GET)
        include_once('../View/Layouts/header.php');
        if(isset($_GET['view'])) { 
            switch($_GET['view'])
            {
                case 'detail':
                    $this->detail($_GET);
                break;
                case 'list':
                    $this->list();
                break;
                case 'add':
                    $this->add();
                break;
                case 'update':
                    $this->update($_GET);
                break;
                case 'delete':
                    $this->delete($_GET);
                break;
                default:
                    $this->error();
                break;
            }
        } else {
            // neu khong co get view thi bao 404 (khong tim thay trang)
            $this->error();
        }
        include_once('../View/Layouts/footer.php');
    }

    public function invokePost() {
        $this->addPost();
        $this->updatePost();
    }

    public function detail($id)
    {
        $modelNews = new NewsModel();
        $news = $modelNews->detail($_GET['new']);

        include_once("../View/News/detail.html");
    }

    public function list()
    {
        $modelNews = new NewsModel();
        $newsList = $modelNews->list();

        include_once("../View/News/list.html");
    }

    public function add() {
        include_one('../View/News/add.html')
    }

    public function addPost() {
        if(isset($_POST['addNews'])) {
            $this->newsModel->save($_POST);
            header('Location:/Controller/NewsController.php?view=list');
        }
    }

    public function update($data)
    {
        if(!isset($data['id'])) {
            header("Location:/Controller/NewsController.php?view=list");
        }

        $news = $this->newsModel->detail($data['id']);
        
        if($news) {
            include_once("../View/News/update.html");
        } else {
            header('Location:/Controller/NewsController.php?view=list');
        }
    }

    public function updatePost()
    {
        if(isset($_POST['updateNews']))
        {
            $this->newsModel->save($_POST);
            header("Location:/Controller/NewsController.php?view=list");
        }
    }
}

$newsController = new NewsController();
$newsController->invoke();
$newsController->invokePost();
?>